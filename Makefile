reqs:
	pip-compile --upgrade --output-file ./requirements.txt ./requirements.in

deploy:
	zappa update production

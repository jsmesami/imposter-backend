import itertools

import boto3
import botocore
from django.conf import settings
from django.core.management import BaseCommand

from imposter.models.poster import Poster
from imposter.models.image import PosterImage
from utils.functional import translate_chars


class Command(BaseCommand):

    help = "Delete documents with no DB references"

    def replace_special(self, s):
        xlat = {
            "'": "&apos;",
            '"': "&quot;",
            '<': "&lt;",
            '>': "&gt;",
            '\r': "&#13;",
            '\n': "&#10;",
        }
        return translate_chars(s.replace('&', "&amp;"), xlat)

    def handle(self, *args, **options):
        S3 = boto3.resource('s3')
        bucket = S3.Bucket(settings.AWS_STORAGE_BUCKET_NAME)

        referenced_docs = itertools.chain.from_iterable(
            o.values() for o in Poster.objects.all().values('thumb', 'print_pdf', 'print_jpg').iterator()
        )
        referenced_images = itertools.chain.from_iterable(
            PosterImage.objects.all().values_list('file', flat=True).iterator()
        )
        referenced_objects = {f'media/{o}' for o in itertools.chain(referenced_docs, referenced_images)}
        bucket_objects = {o.key for o in bucket.objects.filter(Prefix='media/posters/')}
        objects_to_delete = [self.replace_special(o) for o in (bucket_objects - referenced_objects)]

        if len(objects_to_delete):
            client = S3.meta.client
            # TODO: refactor to bulk delete (resolve "invalid XML" errors)
            for o in objects_to_delete:
                try:
                    client.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=o)
                except botocore.exceptions.ClientError as e:
                    print(e)

import base64
import hashlib
import pickle


def urlsafe_hash(*args) -> str:
    h = pickle.dumps(args)
    h = hashlib.sha1(h).digest()[:6]
    h = base64.urlsafe_b64encode(h)
    return h.decode('utf-8', 'ignore')

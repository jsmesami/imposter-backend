import ast
import os


class ImproperlyConfigured(Exception):
    pass


def env(key, default=None):

    value = os.environ.get(key, default)

    try:
        return ast.literal_eval(value)
    except (SyntaxError, ValueError):
        pass

    if value is None:
        raise ImproperlyConfigured(f"Missing required environment variable '{key}'.")

    return value
